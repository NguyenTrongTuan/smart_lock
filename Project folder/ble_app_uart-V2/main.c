/**
 * Copyright (c) 2014 - 2020, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @file
 *
 * @defgroup ble_sdk_uart_over_ble_main main.c
 * @{
 * @ingroup  ble_sdk_app_nus_eval
 * @brief    UART over BLE application main file.
 *
 * This file contains the source code for a sample application that uses the Nordic UART service.
 * This application uses the @ref srvlib_conn_params module.
 */


#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"
#include "app_timer.h"
#include "ble_nus.h"
#include "app_uart.h"
#include "app_util_platform.h"
#include "bsp_btn_ble.h"
#include "nrf_pwr_mgmt.h"

#if defined (UART_PRESENT)
#include "nrf_uart.h"
#endif
#if defined (UARTE_PRESENT)
#include "nrf_uarte.h"
#endif

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "nrf_gpio.h"


#include "nrf_drv_gpiote.h"
#include "app_error.h"
#include "boards.h"



#define APP_BLE_CONN_CFG_TAG            1                                           /**< A tag identifying the SoftDevice BLE configuration. */

#define DEVICE_NAME                     "Nordic_UART"                               /**< Name of device. Will be included in the advertising data. */
#define NUS_SERVICE_UUID_TYPE           BLE_UUID_TYPE_VENDOR_BEGIN                  /**< UUID type for the Nordic UART Service (vendor specific). */

#define APP_BLE_OBSERVER_PRIO           3                                           /**< Application's BLE observer priority. You shouldn't need to modify this value. */

#define APP_ADV_INTERVAL                64                                          /**< The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). */

#define APP_ADV_DURATION                18000                                       /**< The advertising duration (180 seconds) in units of 10 milliseconds. */

#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(20, UNIT_1_25_MS)             /**< Minimum acceptable connection interval (20 ms), Connection interval uses 1.25 ms units. */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(75, UNIT_1_25_MS)             /**< Maximum acceptable connection interval (75 ms), Connection interval uses 1.25 ms units. */
#define SLAVE_LATENCY                   0                                           /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)             /**< Connection supervisory timeout (4 seconds), Supervision Timeout uses 10 ms units. */
#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)                       /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)                      /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           /**< Number of attempts before giving up the connection parameter negotiation. */

#define DEAD_BEEF                       0xDEADBEEF                                  /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define UART_TX_BUF_SIZE                256                                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE                256                                         /**< UART RX buffer size. */

/*******************************************************************************************/
#define   LED_PIN_3     19        //LED 3 LA DIEU KHIEN DONG CO DUNG DE MO CUA
#define   LED_PIN_4     20        //LED 4 LA DIEU KHIEN DONG CO KHOA CUA LAI
#define   BUTTON_PIN_3  15        //NUT BAM SO 3 LA DUNG DE MO TA cam bien hall, neu nhu cua dang dong thi tuc la chan buton o muc THAP
#define   LED_PIN_2     18        //LED 2 la de HIEN THI TRANG THAI CUA KHOA, neu den sang thi cua khoa, den tat thi cua mo
#define   BUTTON_PIN_2  14        //NUT BAM 2 LA DE Mo ta dong co quay het hanh trinh
/*******************************************************************************************/
#define   HALL_SENSOR   5
#define   LED_PIN_3_TD  23//cho nay co the se phai doi thanh 23
#define   LED_PIN_4_TD  24// cho nay co the se phai doi thanh 24
#define   CAM_1         2//CHAN 17
#define   CAM_2         3//CHAN 18


BLE_NUS_DEF(m_nus, NRF_SDH_BLE_TOTAL_LINK_COUNT);                                   /**< BLE NUS service instance. */
NRF_BLE_GATT_DEF(m_gatt);                                                           /**< GATT module instance. */
NRF_BLE_QWR_DEF(m_qwr);                                                             /**< Context for the Queued Write module.*/
BLE_ADVERTISING_DEF(m_advertising);                                                 /**< Advertising module instance. */


/**********************************************************************************/


char command_open[]=      "{cmd:open}\r\n";
char answer_opened[]=     "{status:opened}\r\n";//
char answer_opening[]=    "{status:opening}\r\n";//khoa da mo san roi
char answer_key[]=        "{status:key}\r\n";//khi cua dang dong ma lai co nguoi mo ra
char answer_notcommand[]= "notcomand\r\n";//khi chuoi nhan duoc khong phai cau lenh
char answer_locked[]=     "{status:locked}\r\n";
char answer_opened_while1[]= "opened_while1\r\n";
char answer_locked_in_interrupt[]= "locked_interrupt\r\n";
char buffer[20];


uint8_t flag_status = 0; //trang thai gan nhat cua canh cua la mo hay khoa, neu la 0 --> dang mo, neu la 1 --> dang khoa
uint8_t flag_waiting = 0;
uint32_t pin_state_pin_1 = 0;
uint32_t pin_state_pin_2 = 0;
ret_code_t err_code_INTERRUPT;
/**********************************************************************************/


static uint16_t   m_conn_handle          = BLE_CONN_HANDLE_INVALID;                 /**< Handle of the current connection. */
static uint16_t   m_ble_nus_max_data_len = BLE_GATT_ATT_MTU_DEFAULT - 3;            /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */
static ble_uuid_t m_adv_uuids[]          =                                          /**< Universally unique service identifier. */
{
    {BLE_UUID_NUS_SERVICE, NUS_SERVICE_UUID_TYPE}
};


/**@brief Function for assert macro callback.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyse
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num    Line number of the failing ASSERT call.
 * @param[in] p_file_name File name of the failing ASSERT call.
 */

void Delay_Not_Accurate(uint32_t time)
{
  uint32_t i =0;
  uint32_t j =0;
  for(i = 0; i < time; i++)
  {
    for(j = 0; j < 10000; j++){;}
  }
}
void nrf_gpio_pin_set_ntt(uint32_t pin_number)
{
  NRF_GPIO_Type * reg;// = nrf_gpio_pin_port_decode(&pin_number);
/*************************************************************************
  if (pin_number < P0_PIN_NUM)
    {
        reg = NRF_P0;
    }
    else
    {
        pin_number = pin_number & 0x1F;
        reg = NRF_P1;
    }
***********************************************************************/  
if (GPIO_COUNT == 1)
{
  reg = NRF_P0;
}  

//  nrf_gpio_port_out_set(reg, 1UL << pin_number);
   reg->OUTSET = 1UL << pin_number;

}
void nrf_gpio_pin_clear_ntt(uint32_t pin_number)
{
  NRF_GPIO_Type * reg;// = nrf_gpio_pin_port_decode(&pin_number);
/***************************************************************************
  //if (pin_number < P0_PIN_NUM)
  //  {
  //      reg = NRF_P0;
  //  }
  //  else
  //  {
  //      pin_number = pin_number & 0x1F;
  //      reg = NRF_P1;
  //  }
****************************************************************************/
if (GPIO_COUNT == 1)
{
  reg = NRF_P0;
}
//  nrf_gpio_port_out_set(reg, 1UL << pin_number);
   reg->OUTCLR = 1UL << pin_number;

}
void led_sensor_init(void)
{
  nrf_gpio_cfg_input(BUTTON_PIN_3,NRF_GPIO_PIN_PULLUP);
  nrf_gpio_cfg_output(LED_PIN_3);
  nrf_gpio_pin_clear(LED_PIN_3);
  nrf_gpio_cfg_output(LED_PIN_2);//cho nay khong can thiet boi vi led2 da duoc khai bao o ham khoi tao ngat Interrupt_event_init()
  nrf_gpio_pin_clear(LED_PIN_2);//va no se duoc config luon gia tri ban dau, tuy vao truong du lieu ma minh nhap vao bien out_config, xem buc anh (**) ben Onenote de biet them chi tiet
  nrf_gpio_cfg_output(LED_PIN_4);
  nrf_gpio_pin_clear(LED_PIN_4);
  nrf_gpio_cfg_input(BUTTON_PIN_2,NRF_GPIO_PIN_PULLUP);

  nrf_gpio_cfg_input(CAM_1,NRF_GPIO_PIN_PULLUP);
  nrf_gpio_cfg_input(CAM_2,NRF_GPIO_PIN_PULLUP);

  //nrf_gpio_cfg_output(LED_PIN_3_TD);
  //nrf_gpio_cfg_output(LED_PIN_4_TD);

}
          //void set_led_3_to_high(uint32_t Pin_Number)
          //{
          //  NRF_GPIO_Type * reg        = nrf_gpio_pin_port_decode(&Pin_Number);
          //  uint32_t        pins_state = 0x1C040;//GIA TRI nay la gia tri bat LED3
          ////  pin_state_pin_1 = pins_state;
          //  reg->OUTSET = (~pins_state & (1UL << Pin_Number));
          //  reg->OUTCLR = (pins_state & (1UL << Pin_Number));
          //}

          //void set_led_3_to_low(uint32_t Pin_Number)
          //{
          //  NRF_GPIO_Type * reg        = nrf_gpio_pin_port_decode(&Pin_Number);
          //  uint32_t        pins_state = 0x14040;//gia tri nay la gia tri tat LED3
          ////  pin_state_pin_2 = pins_state;
          //  reg->OUTSET = (~pins_state & (1UL << Pin_Number));
          //  reg->OUTCLR = (pins_state & (1UL << Pin_Number));
          //}
          //void set_led_2_to_high(uint32_t Pin_Number)
          //{
          //  NRF_GPIO_Type * reg        = nrf_gpio_pin_port_decode(&Pin_Number);
          //  uint32_t        pins_state = 0x1C040;//GIA TRI nay la gia tri bat LED2
          //  pin_state_pin_1 = pins_state;
          //  reg->OUTSET = (~pins_state & (1UL << Pin_Number));
          //  reg->OUTCLR = (pins_state & (1UL << Pin_Number));
          //}

          //void set_led_2_to_low(uint32_t Pin_Number)
          //{
          //  NRF_GPIO_Type * reg        = nrf_gpio_pin_port_decode(&Pin_Number);
          //  uint32_t        pins_state = 0x18040;//gia tri nay la gia tri tat LED2
          //  pin_state_pin_2 = pins_state;
          //  reg->OUTSET = (~pins_state & (1UL << Pin_Number));
          //  reg->OUTCLR = (pins_state & (1UL << Pin_Number));
          //}

uint32_t Read_Pin(uint32_t Pin_Number)
{
  
    NRF_GPIO_Type * reg = nrf_gpio_pin_port_decode(&Pin_Number);

    return (((reg->IN) >> Pin_Number) & 1UL);

}


void interrupt_event_handle(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)//ham xu ly ngat
{
    //vao chuong trinh phuc vu ngat nay co nghia la co mot xung suon LEN o chan BUTTON_PIN_3, dieu do co nghia la cua dang mo thi bi dong lai 
//  Delay_Not_Accurate(100);
  //if(Read_Pin(BUTTON_PIN_3) == 0)//KHI CUA DONG LAI THI CAM BIEN hALL SE xuong muc 0, CUA MO RA THI NO SE O MUC 1,neu sau 600s ma no van o muc 0 thi se tien hanh khoa
  //{
    //cho dong co quay khoa cua lai
//******************************************************************************************//
//cho nay comment lai boi vi khong can thiet phai xet co khi o trong ngat, dieu kien de co ngat la phai cua dang mo thi dong lai ma
    if(flag_status == 1)//trang thai cua dang khoa, gui lai chuoi answer_locked
    {
      //do
      //  {
        
      //    uint16_t _length_ = (uint16_t)strlen(answer_locked);//cho nay la no thuc hien ham xu ly chuoi 
      //    err_code_INTERRUPT = ble_nus_data_send(&m_nus,answer_locked, &_length_, m_conn_handle);//day la ham gui tu Peripheral (server) ve phia central(client)
      //    if ((err_code_INTERRUPT != NRF_ERROR_INVALID_STATE) &&
      //        (err_code_INTERRUPT != NRF_ERROR_RESOURCES) &&
      //        (err_code_INTERRUPT != NRF_ERROR_NOT_FOUND))
      //    {
      //        APP_ERROR_CHECK(err_code_INTERRUPT);
      //    }
      //  } 
      //while (err_code_INTERRUPT == NRF_ERROR_RESOURCES);
    }
    else
    {
//************************************************************************************/
      nrf_gpio_pin_write(LED_PIN_4,0);//Dieu khien dong co khoa cua lai
      nrf_gpio_pin_clear_ntt(LED_PIN_4_TD);
      nrf_gpio_pin_set_ntt(LED_PIN_3_TD);

      //while(Read_Pin(BUTTON_PIN_2) == 0);//khi BUTTON_PIN_2  = 0 thi tuc la chot chua dong, cua van dang mo
      while(nrf_gpio_pin_read(CAM_2) == 1);

      nrf_gpio_pin_write(LED_PIN_4,1);//DIEU khien dong co dung lai
      nrf_gpio_pin_clear_ntt(LED_PIN_4_TD);
      nrf_gpio_pin_clear_ntt(LED_PIN_3_TD);

      flag_status = 1;//co bao cua da khoa, tam thoi se hien thi bang den lED 2
      nrf_gpio_pin_write(LED_2,0);//bat den led 2 len de bao trang thai la da khoa

  //**************************************************************************************/

          do//thong bao la cua da khoa lai
            {
              uint16_t _length_ = (uint16_t)strlen(answer_locked_in_interrupt);//cho nay la no thuc hien ham xu ly chuoi 
              err_code_INTERRUPT = ble_nus_data_send(&m_nus,answer_locked_in_interrupt, &_length_, m_conn_handle);//day la ham gui tu Peripheral (server) ve phia central(client)
              if ((err_code_INTERRUPT != NRF_ERROR_INVALID_STATE) &&
                  (err_code_INTERRUPT != NRF_ERROR_RESOURCES) &&
                  (err_code_INTERRUPT != NRF_ERROR_NOT_FOUND))
              {
                  APP_ERROR_CHECK(err_code_INTERRUPT);
              }
            } 
           while (err_code_INTERRUPT == NRF_ERROR_RESOURCES);

  /**************************************************************************************/
  //  }

    }
}


void Interrup_Event_Init()
{

    ret_code_t err_code; // hold error value


    if (!nrf_drv_gpiote_is_init())
    {
        err_code = nrf_drv_gpiote_init();
        APP_ERROR_CHECK(err_code);
    }
    else
    {
      //err_code = nrf_drv_gpiote_init(); // Initialize the GPIOTE
      //VERIFY_SUCCESS(err_code); // check for the errors
    }


    //nrf_gpio_cfg_output(LED_PIN_2); // Initialize the Led 
    //nrf_gpio_pin_clear(LED_PIN_2); // turn off the led
    nrf_drv_gpiote_out_config_t out_config = GPIOTE_CONFIG_OUT_SIMPLE(false);

    //err_code = nrf_drv_gpiote_out_init(LED_PIN_2, &out_config);
    //APP_ERROR_CHECK(err_code);
    //err_code = nrf_drv_gpiote_out_init(LED_PIN_4, &out_config);
    //APP_ERROR_CHECK(err_code);
    err_code = nrf_drv_gpiote_out_init(LED_PIN_3_TD, &out_config);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_drv_gpiote_out_init(LED_PIN_4_TD, &out_config);
    APP_ERROR_CHECK(err_code);

    //nrf_gpio_pin_write(LED_PIN_2,1); // turn off the led
    //nrf_gpio_pin_write(LED_PIN_4,1); // turn off the led
    nrf_drv_gpiote_in_config_t  in_config = GPIOTE_CONFIG_IN_SENSE_HITOLO(true); // RAISING edge interrupt detection
    in_config.pull = NRF_GPIO_PIN_PULLUP;

    err_code = nrf_drv_gpiote_in_init(HALL_SENSOR, &in_config, interrupt_event_handle); // Initialize the interrupt pin 
    APP_ERROR_CHECK(err_code);

    nrf_drv_gpiote_in_event_enable(HALL_SENSOR, true); // Enable the interrupt events
}



void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

/**@brief Function for initializing the timer module.
 */
static void timers_init(void)
{
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for the GAP initialization.
 *
 * @details This function will set up all the necessary GAP (Generic Access Profile) parameters of
 *          the device. It also sets the permissions and appearance.
 */
static void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *) DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling Queued Write Module errors.
 *
 * @details A pointer to this function will be passed to each service which may need to inform the
 *          application about an error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void nrf_qwr_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for handling the data from the Nordic UART Service.
 *
 * @details This function will process the data received from the Nordic UART BLE Service and send
 *          it to the UART module.
 *
 * @param[in] p_evt       Nordic UART Service event.
 */
/**@snippet [Handling the data received over BLE] */
static void nus_data_handler(ble_nus_evt_t * p_evt)////////ham nay co phai la nhan chuoi ki tu qua ble xong roi truyen di dau nhi?? truyen len may tinh hay la truyen nguoc lai qua ble
{
//ham nay la ham nhan chuoi ki tu tu phia central (client) roi gui no len may tinh thong qua UART bang Hercules
    if (p_evt->type == BLE_NUS_EVT_RX_DATA)
    {
        uint32_t err_code;

        NRF_LOG_DEBUG("Received data from BLE NUS. Writing data on UART.");
        NRF_LOG_HEXDUMP_DEBUG(p_evt->params.rx_data.p_data, p_evt->params.rx_data.length);

        for (uint32_t i = 0; i < p_evt->params.rx_data.length; i++)
        {
            do
              {
                err_code = app_uart_put(p_evt->params.rx_data.p_data[i]);
                buffer[i] = p_evt->params.rx_data.p_data[i];
                //printf("%c",p_evt->params.rx_data.p_data[i]);//////////////////////////them vao
                if ((err_code != NRF_SUCCESS) && (err_code != NRF_ERROR_BUSY))
                {
                    NRF_LOG_ERROR("Failed receiving NUS message. Error 0x%x. ", err_code);
                    APP_ERROR_CHECK(err_code);
                }
              } 
            while (err_code == NRF_ERROR_BUSY);

        }
        if (p_evt->params.rx_data.p_data[p_evt->params.rx_data.length - 1] == '\r')
        {
            while (app_uart_put('\n') == NRF_ERROR_BUSY);
            //printf("\n");//////////////them vao
        }

/****************************************************************them vao*********************************/
//        strcpy(buffer,p_evt->params.rx_data.p_data);
        if(!strcmp(buffer,command_open))//neu nhu cau lenh nhan duoc la lenh yeu cau mo cua
        {
          //if(Read_Pin(BUTTON_PIN_3) == 0)//  neu nhu cua dang khoa, tuc la tin hieu cua Hall vao chan Button dang o muc THAP
          
          if(flag_status == 1)//neu nhu cua dang khoa
          {//SAU NAY TA CO the thay the viec doc trang thai cam bien Hall bang cach xet co Flag


            //nrf_gpio_pin_set(LED_PIN_3);//HAM nay tuong trung cho viec dieu khien dong co mo cua ra

            nrf_gpio_pin_write(LED_3,0);//dieu khien mo cua
            nrf_gpio_pin_set(LED_PIN_4_TD);
            nrf_gpio_pin_clear(LED_PIN_3_TD);
            //nrf_gpio_pin_set_ntt(LED_PIN_3_TD);
            //nrf_gpio_pin_clear_ntt(LED_PIN_4_TD);

            //Delay_Not_Accurate(1000);
            
            
            //while(nrf_gpio_pin_read(BUTTON_PIN_2) == 1);//cho cho den khi nao cam bien vi tri xuong muc 0, tuc la dong co quay het hanh trinh, khi cua dang dong thi muc logic = 1
            while(nrf_gpio_pin_read(CAM_1) == 1);
            
            
            nrf_gpio_pin_write(LED_3,1);//ket thuc mo cua
            nrf_gpio_pin_clear(LED_PIN_3_TD);
            nrf_gpio_pin_clear(LED_PIN_4_TD);
            //nrf_gpio_pin_clear_ntt(LED_PIN_3_TD);
            //nrf_gpio_pin_set_ntt(LED_PIN_4_TD);

             flag_waiting = 1;//co bao trang thai cho
             flag_status = 0;//co bao trang thai cua mo, ta co the dung co nay thay vi phai dung led2, flag = 0 -->cua mo

            //set_led_2_to_low(LED_PIN_2);//DEN tat --> khoa da mo
            nrf_gpio_pin_write(LED_2, 1);//muon xem tham so cua ham nay thi vao note xem, led2 tat di de bao hieu la da mo cua
            //Delay_Not_Accurate(200);
            //nrf_gpio_pin_write(LED_4, 1);

            //set_led_2_to_low(LED_PIN_2);
            //while(nrf_gpio_pin_read(BUTTON_PIN_3) == 1);//cho cho den khi nao cam bien xuong muc 0, tuc la cua da mo ra
            //nrf_gpio_pin_toggle(LED_PIN_3);//tat dong co di
            do
              {
                uint16_t _length_ = (uint16_t)strlen(answer_opened);//cho nay la no thuc hien ham xu ly chuoi 
                err_code = ble_nus_data_send(&m_nus,answer_opened, &_length_, m_conn_handle);//day la ham gui tu Peripheral (server) ve phia central(client)
                if ((err_code != NRF_ERROR_INVALID_STATE) &&
                    (err_code != NRF_ERROR_RESOURCES) &&
                    (err_code != NRF_ERROR_NOT_FOUND))
                {
                    APP_ERROR_CHECK(err_code);
                }
              } 
             while (err_code == NRF_ERROR_RESOURCES);

          }
          else//cam bien Hall = 1, Flag_status == 0, tuc la neu nhu cua dang mo san roi
          {
            do
              {
                uint16_t _length_ = (uint16_t)strlen(answer_opening);//cho nay la no thuc hien ham xu ly chuoi 
                err_code = ble_nus_data_send(&m_nus,answer_opening, &_length_, m_conn_handle);//day la ham gui tu Peripheral (server) ve phia central(client)
                if ((err_code != NRF_ERROR_INVALID_STATE) &&
                    (err_code != NRF_ERROR_RESOURCES) &&
                    (err_code != NRF_ERROR_NOT_FOUND))
                {
                    APP_ERROR_CHECK(err_code);
                }
              } 
            while (err_code == NRF_ERROR_RESOURCES);
            flag_status = 0;//co bao trang thai cua, ta co the dung co bay thay vi dung LED
            nrf_gpio_pin_write(LED_PIN_2,1);// tat led 2 di, -- cua mo
            
          }
          //printf("gia tri=%ld",pin_state_pin_1);
          //printf("gia tri=%ld",pin_state_pin_2);
        }
        else//day la du phong cho truonG hop co mot cau lenh khac duoc gui tu phia ESP32
        {
          do
            {
              uint16_t _length_ = (uint16_t)strlen(answer_notcommand);//cho nay la no thuc hien ham xu ly chuoi 
              err_code = ble_nus_data_send(&m_nus,answer_notcommand, &_length_, m_conn_handle);//day la ham gui tu Peripheral (server) ve phia central(client)
              if ((err_code != NRF_ERROR_INVALID_STATE) &&
                  (err_code != NRF_ERROR_RESOURCES) &&
                  (err_code != NRF_ERROR_NOT_FOUND))
              {
                  APP_ERROR_CHECK(err_code);
              }
            } 
          while (err_code == NRF_ERROR_RESOURCES);

        }
        //for(uint16_t count = 0; count <= (uint16_t)strlen(p_evt->params.rx_data.length) ; count ++)//day la buoc xoa mang
        //{
        //  p_evt->params.rx_data.p_data[count] = '\0';
        //}

        for(uint16_t count = 0; count <= (uint16_t)strlen(buffer) ; count ++)//day la buoc xoa mang
        {
          buffer[count] = '\0';
        }

 //*/
/***********************************************************************************************/
    }

}
/**@snippet [Handling the data received over BLE] */


/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
    uint32_t           err_code;
    ble_nus_init_t     nus_init;
    nrf_ble_qwr_init_t qwr_init = {0};

    // Initialize Queued Write Module.
    qwr_init.error_handler = nrf_qwr_error_handler;

    err_code = nrf_ble_qwr_init(&m_qwr, &qwr_init);
    APP_ERROR_CHECK(err_code);

    // Initialize NUS.
    memset(&nus_init, 0, sizeof(nus_init));

    nus_init.data_handler = nus_data_handler;

    err_code = ble_nus_init(&m_nus, &nus_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling an event from the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module
 *          which are passed to the application.
 *
 * @note All this function does is to disconnect. This could have been done by simply setting
 *       the disconnect_on_fail config parameter, but instead we use the event handler
 *       mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for handling errors from the Connection Parameters module.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
static void sleep_mode_enter(void)
{
    uint32_t err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
    err_code = bsp_btn_ble_sleep_mode_prepare();
    APP_ERROR_CHECK(err_code);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    uint32_t err_code;

    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);//day la cho ma dang ket noi thi den LED se nhap nhay
            APP_ERROR_CHECK(err_code);
            break;
        case BLE_ADV_EVT_IDLE:
            sleep_mode_enter();
            break;
        default:
            break;
    }
}


/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    uint32_t err_code;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_INFO("Connected");
            err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
            APP_ERROR_CHECK(err_code);
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            err_code = nrf_ble_qwr_conn_handle_assign(&m_qwr, m_conn_handle);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_INFO("Disconnected");
            // LED indication will be changed when advertising starts.
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            // Pairing not supported
            err_code = sd_ble_gap_sec_params_reply(m_conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
            // No system attributes have been stored.
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for the SoftDevice initialization.
 *
 * @details This function initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}


/**@brief Function for handling events from the GATT library. */
void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt)
{
    if ((m_conn_handle == p_evt->conn_handle) && (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED))
    {
        m_ble_nus_max_data_len = p_evt->params.att_mtu_effective - OPCODE_LENGTH - HANDLE_LENGTH;
        NRF_LOG_INFO("Data len is set to 0x%X(%d)", m_ble_nus_max_data_len, m_ble_nus_max_data_len);
    }
    NRF_LOG_DEBUG("ATT MTU exchange completed. central 0x%x peripheral 0x%x",
                  p_gatt->att_mtu_desired_central,
                  p_gatt->att_mtu_desired_periph);
}


/**@brief Function for initializing the GATT library. */
void gatt_init(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_gatt_att_mtu_periph_set(&m_gatt, NRF_SDH_BLE_GATT_MAX_MTU_SIZE);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated by button press.
 */
void bsp_event_handler(bsp_event_t event)//day la cho ma mach se vao che do sleep ngu sau khi doi lau qua
{
    uint32_t err_code;
    switch (event)
    {
        case BSP_EVENT_SLEEP:
            sleep_mode_enter();
            break;

        case BSP_EVENT_DISCONNECT:
            err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            if (err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
            break;

        case BSP_EVENT_WHITELIST_OFF:
            if (m_conn_handle == BLE_CONN_HANDLE_INVALID)
            {
                err_code = ble_advertising_restart_without_whitelist(&m_advertising);
                if (err_code != NRF_ERROR_INVALID_STATE)
                {
                    APP_ERROR_CHECK(err_code);
                }
            }
            break;

        default:
            break;
    }
}


/**@brief   Function for handling app_uart events.
 *
 * @details This function will receive a single character from the app_uart module and append it to
 *          a string. The string will be be sent over BLE when the last character received was a
 *          'new line' '\n' (hex 0x0A) or if the string has reached the maximum data length.
 */
/**@snippet [Handling the data received over UART] */
void uart_event_handle(app_uart_evt_t * p_event)/////////////ham nay co phai la nhan ki tu truyen xuong tu may tinh qua UART roi truyen len dien tho?i qua BLE ko?
{//ham nay la ham nhan chuoi ki tu bang UART qua Hercules roi gui den central (client) qua ble
 //   static uint8_t data_array[BLE_NUS_MAX_DATA_LEN];
 //   static uint8_t index = 0;
 //   uint32_t       err_code;

 //   switch (p_event->evt_type)
 //   {
 //       case APP_UART_DATA_READY:
 //           UNUSED_VARIABLE(app_uart_get(&data_array[index]));
 //           index++;

 //           if ((data_array[index - 1] == '\n') ||
 ////               (data_array[index - 1] == '\r') ||
 //               (index >= m_ble_nus_max_data_len))
 //           {
 //               if (index > 1)
 //               {
 //                   NRF_LOG_DEBUG("Ready to send data over BLE NUS");
 //                   NRF_LOG_HEXDUMP_DEBUG(data_array, index);

 //                   do
 //                   {
 //                       uint16_t length = (uint16_t)index;//cho nay la no thuc hien ham xu ly chuoi 
 //                       err_code = ble_nus_data_send(&m_nus, data_array, &length, m_conn_handle);//day la ham gui tu Peripheral (server) ve phia central(client)
 //                       if ((err_code != NRF_ERROR_INVALID_STATE) &&
 //                           (err_code != NRF_ERROR_RESOURCES) &&
 //                           (err_code != NRF_ERROR_NOT_FOUND))
 //                       {
 //                           APP_ERROR_CHECK(err_code);
 //                       }
 //                   } while (err_code == NRF_ERROR_RESOURCES);
 //               }

 //               index = 0;
 //           }
 //           break;

 //       case APP_UART_COMMUNICATION_ERROR:
 //           APP_ERROR_HANDLER(p_event->data.error_communication);
 //           break;

 //       case APP_UART_FIFO_ERROR:
 //           APP_ERROR_HANDLER(p_event->data.error_code);
 //           break;

 //       default:
 //           break;
 //   }
}
/**@snippet [Handling the data received over UART] */


/**@brief  Function for initializing the UART module.
 */
/**@snippet [UART Initialization] */
static void uart_init(void)
{
    uint32_t                     err_code;
    app_uart_comm_params_t const comm_params =
    {
        .rx_pin_no    = RX_PIN_NUMBER,
        .tx_pin_no    = TX_PIN_NUMBER,
        .rts_pin_no   = RTS_PIN_NUMBER,
        .cts_pin_no   = CTS_PIN_NUMBER,
        .flow_control = APP_UART_FLOW_CONTROL_DISABLED,
        .use_parity   = false,
#if defined (UART_PRESENT)
        .baud_rate    = NRF_UART_BAUDRATE_115200
#else
        .baud_rate    = NRF_UARTE_BAUDRATE_115200
#endif
    };

    APP_UART_FIFO_INIT(&comm_params,
                       UART_RX_BUF_SIZE,
                       UART_TX_BUF_SIZE,
                       uart_event_handle,
                       APP_IRQ_PRIORITY_LOWEST,
                       err_code);
    APP_ERROR_CHECK(err_code);
}
/**@snippet [UART Initialization] */


/**@brief Function for initializing the Advertising functionality.
 */
static void advertising_init(void)
{
    uint32_t               err_code;
    ble_advertising_init_t init;

    memset(&init, 0, sizeof(init));

    init.advdata.name_type          = BLE_ADVDATA_FULL_NAME;
    init.advdata.include_appearance = false;
    init.advdata.flags              = BLE_GAP_ADV_FLAGS_LE_ONLY_LIMITED_DISC_MODE;

    init.srdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    init.srdata.uuids_complete.p_uuids  = m_adv_uuids;

    init.config.ble_adv_fast_enabled  = true;
    init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
    init.config.ble_adv_fast_timeout  = APP_ADV_DURATION;
    init.evt_handler = on_adv_evt;

    err_code = ble_advertising_init(&m_advertising, &init);
    APP_ERROR_CHECK(err_code);

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
}


/**@brief Function for initializing buttons and leds.
 *
 * @param[out] p_erase_bonds  Will be true if the clear bonding button was pressed to wake the application up.
 */
static void buttons_leds_init(bool * p_erase_bonds)
{
    bsp_event_t startup_event;

    uint32_t err_code = bsp_init(BSP_INIT_LEDS | BSP_INIT_BUTTONS, bsp_event_handler);
    APP_ERROR_CHECK(err_code);

    err_code = bsp_btn_ble_init(NULL, &startup_event);
    APP_ERROR_CHECK(err_code);

    *p_erase_bonds = (startup_event == BSP_EVENT_CLEAR_BONDING_DATA);
}


/**@brief Function for initializing the nrf log module.
 */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


/**@brief Function for initializing power management.
 */
static void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the idle state (main loop).
 *
 * @details If there is no pending log operation, then sleep until next the next event occurs.
 */
static void idle_state_handle(void)
{
    if (NRF_LOG_PROCESS() == false)
    {
        nrf_pwr_mgmt_run();
    }
}


/**@brief Function for starting advertising.
 */
static void advertising_start(void)
{
    uint32_t err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
    APP_ERROR_CHECK(err_code);
}


/**@brief Application main function.
 */
int main(void)
{
    bool erase_bonds;
    ret_code_t err_code_val_in_main;
//    Interrup_Event_Init();
    led_sensor_init();
    Interrup_Event_Init();
    // Initialize.
    uart_init();
    log_init();
    timers_init();
    buttons_leds_init(&erase_bonds);
    power_management_init();
    ble_stack_init();
    gap_params_init();
    gatt_init();
    services_init();
    advertising_init();
    conn_params_init();
    // Start execution.
    printf("\r\nUART started.\r\n");
//    NRF_LOG_FLUSH();
    NRF_LOG_INFO("Debug logging for UART over RTT started.");
    
    advertising_start();
//    printf("\r\nUART started 111.\r\n");

//    Interrup_Event_Init();
    for (;;)
    {
        idle_state_handle();
         
        if( (nrf_gpio_pin_read(BUTTON_PIN_3) == 0) && (flag_waiting == 1) )//neu nhu cua dang o trang thai khep lai 
        {
          
          Delay_Not_Accurate(2000);//xem lai cai thoi gian nay da chuan chua
          if(nrf_gpio_pin_read(BUTTON_PIN_3) == 0)//neu nhu cua van dang o trang thai khep ma co flag van bao la dang mo (khong vao ngat)
          {
            nrf_gpio_pin_write(LED_PIN_4,0);//Dieu khien dong co khoa cua lai
            nrf_gpio_pin_clear_ntt(LED_PIN_4_TD);
            nrf_gpio_pin_set_ntt(LED_PIN_3_TD);

            //while(Read_Pin(BUTTON_PIN_2) == 0);//khi BUTTON_PIN_2  = 0 thi tuc la chot chua dong, cua van dang mo
            while(nrf_gpio_pin_read(CAM_2) == 1);

            nrf_gpio_pin_write(LED_PIN_4,1);//DIEU khien dong co dung lai
            nrf_gpio_pin_clear_ntt(LED_PIN_4_TD);
            nrf_gpio_pin_clear_ntt(LED_PIN_3_TD);

            flag_status = 1;//co bao cua da khoa, tam thoi se hien thi bang den lED 2
            nrf_gpio_pin_write(LED_2,0);//bat den led 2 len de bao trang thai la da khoa

        /**************************************************************************************/

            do
              {
        
                uint16_t _length_ = (uint16_t)strlen(answer_locked);//cho nay la no thuc hien ham xu ly chuoi 
                err_code_val_in_main = ble_nus_data_send(&m_nus,answer_locked, &_length_, m_conn_handle);//day la ham gui tu Peripheral (server) ve phia central(client)
                if ((err_code_val_in_main != NRF_ERROR_INVALID_STATE) &&
                    (err_code_val_in_main != NRF_ERROR_RESOURCES) &&
                    (err_code_val_in_main != NRF_ERROR_NOT_FOUND))
                {
                    APP_ERROR_CHECK(err_code_val_in_main);
                }
              } 
            while (err_code_val_in_main == NRF_ERROR_RESOURCES);

        /**************************************************************************************/
        //  }
            
          }
          else
          {
            //sau khi delay xong ma thay cua da mo ra hoac la cua da duoc dong lai do nhay vao ngat thi thoi khong khoa lai nua
            
          }
          flag_waiting = 0;//tat co flag_waiting di, boi vi cho du sau khi delay, cua co bi khoa hay khong di chang nua thi ngay sau do,neu cua bi dong lai thi no se duoc xu ly o trong ngat chu khong phai trong while(1)
          
        }

        if( (flag_status == 1) && (nrf_gpio_pin_read(BUTTON_PIN_3) != 0) )//neu nhu cua dang mo ma co Flag lai dang thong bao la dang dong
        {
          flag_status = 0;//dat lai gia tri cua co flag bao hieu la da mo cua
          do//thong bao la cua da mo ra
            {
              uint16_t _length_ = (uint16_t)strlen(answer_key);//cho nay la no thuc hien ham xu ly chuoi 
              err_code_val_in_main = ble_nus_data_send(&m_nus,answer_key, &_length_, m_conn_handle);//day la ham gui tu Peripheral (server) ve phia central(client)
              if ((err_code_val_in_main != NRF_ERROR_INVALID_STATE) &&
                  (err_code_val_in_main != NRF_ERROR_RESOURCES) &&
                  (err_code_val_in_main != NRF_ERROR_NOT_FOUND))
              {
                  APP_ERROR_CHECK(err_code_val_in_main);
              }
            } 
           while (err_code_val_in_main == NRF_ERROR_RESOURCES);
        }



      //nrf_gpio_pin_set(LED_PIN_3_TD);
      //nrf_gpio_pin_clear(LED_PIN_4_TD);


    }
}


/**
 * @}
 */
